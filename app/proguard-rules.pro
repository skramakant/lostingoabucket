# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/ramakant/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Add this global rule
-keepattributes Signature

# This rule will properly ProGuard all the model classes in
# the package com.yourcompany.models. Modify to fit the structure
# of your app.


 # /////////////////////////// Rules of proguard ///////////////////////////////////////////////////////



# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\aaron\AppData\Local\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-dontpreverify
-ignorewarnings

-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-forceprocessing
-verbose    # print error logs

# Optimization is turned off by default. Dex does not like code run
# through the ProGuard optimize and preverify steps (and performs some
# of these optimizations on its own).
#-dontoptimize
#-dontpreverify

# If you want to enable optimization, you should include the
# following:
-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*, !code/allocation/variable
-optimizationpasses 5
-allowaccessmodification
#-repackageclasses ''


-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgent
-keep public class * extends android.preference.Preference
-keep public class * extends android.support.v4.app.Fragment
-keep public class * extends android.support.v4.app.DialogFragment
-keep public class * extends android.app.Fragment
#-keep public class com.android.vending.licensing.ILicensingService
-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }


-keep class com.lostingoa.lostingoa.Banks.** { *; }
-keep class com.lostingoa.lostingoa.Bike.** { *; }
-keep class com.lostingoa.lostingoa.Classifieds.** { *; }

-keep class com.lostingoa.lostingoa.Database.** { *; }
-keep class com.lostingoa.lostingoa.EmergencyModule.** { *; }
-keep class com.lostingoa.lostingoa.Entertainment.** { *; }
-keep class com.lostingoa.lostingoa.Feed.** { *; }
-keep class com.lostingoa.lostingoa.Feedback.** { *; }
-keep class com.lostingoa.lostingoa.PostOffice.** { *; }
-keep class com.lostingoa.lostingoa.Repair.** { *; }
-keep class com.lostingoa.lostingoa.SafetyPolice.** { *; }
-keep class com.lostingoa.lostingoa.SnakeRescuer.** { *; }

-keep class com.lostingoa.lostingoa.Taxi.** { *; }
-keep class com.lostingoa.lostingoa.Transport.** { *; }







-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** v(...);
}

-keep class com.lostingoa.lostingoa.**
-keep class com.google.firebase.**
-keep class com.firebase.**

#-keep class com.google.android.gms.analytics.** { *; }

#-keep class com.crashlytics.** { *; }



#-dontwarn com.google.android.gms.**
#-dontwarn com.crashlytics.**

############################################################################################
-keepclassmembers class com.lostingoa.lostingoa.EmergencyModule.EmergencyModel {
  *;
}
-keep class com.lostingoa.lostingoa.EmergencyModule.EmergencyModel {
  *;
}
-keepclassmembers class com.lostingoa.lostingoa.EmergencyModule.EmergencyDetailsModel {
  *;
}
-keep class com.lostingoa.lostingoa.EmergencyModule.EmergencyDetailsModel {
  *;
}
-keepclassmembers class com.lostingoa.lostingoa.SnakeRescuer.SnakeRescuerTitleModel {
  *;
}

-keep class com.lostingoa.lostingoa.EmergencyModule.EmergencyViewHolder.** {
  *;
}

-keep class com.instamojo.android.**{*;}