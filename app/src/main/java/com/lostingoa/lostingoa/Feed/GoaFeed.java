package com.lostingoa.lostingoa.Feed;

import android.content.Intent;
import android.net.Network;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.lostingoa.lostingoa.Database.DatabaseInstance;
import com.lostingoa.lostingoa.Database.DatabaseUtils;
import com.lostingoa.lostingoa.EmergencyModule.EmergencyDetailsModel;
import com.lostingoa.lostingoa.EmergencyModule.EmergencyDetailsViewHolder;
import com.lostingoa.lostingoa.LostInGoaApplication;
import com.lostingoa.lostingoa.MapView;
import com.lostingoa.lostingoa.R;
import com.lostingoa.lostingoa.Utils;

import java.text.DateFormat;
import java.util.Date;
import java.util.Map;

public class GoaFeed extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView rvGoaFeedDetailsList;
    private EditText etFeedBack;
    private Button btSend;
    private DatabaseReference mGoaFeedReference;
    private FirebaseRecyclerAdapter mFirebaseAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goa_feed);
        getSupportActionBar().setTitle("Goafeeds");
        initializeViews();
        if(LostInGoaApplication.getFromSharedPreference(this, Utils.USER_HANDLE_KEY).equals(Utils.DEFAULT_HANDLE)){
            showDialogForUserHandle();
        }
        mGoaFeedReference = DatabaseInstance.getFirebaseInstance().getReference().child(DatabaseUtils.GOA_FEED_TABLE_NAME);
        setUpFirebaseAdapter();

    }

    private void showDialogForUserHandle() {
        Intent intent = new Intent(this,UserHandleForFeed.class);
        startActivity(intent);
    }

    private void setUpFirebaseAdapter() {
        LostInGoaApplication.showProgressDialog(this);
        mFirebaseAdapter = new FirebaseRecyclerAdapter<GoaFeedModel,GoaFeedViewHolder>(GoaFeedModel.class, R.layout.goa_feed_list_item, GoaFeedViewHolder.class,
                mGoaFeedReference) {

            @Override
            protected void populateViewHolder(GoaFeedViewHolder viewHolder, GoaFeedModel model, int position) {
                viewHolder.bindEmergency(model);
            }
        };
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        rvGoaFeedDetailsList.setLayoutManager(linearLayoutManager);
        rvGoaFeedDetailsList.setAdapter(mFirebaseAdapter);
        mGoaFeedReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                LostInGoaApplication.hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        //scrollToTheLastPosition();
    }

    private void scrollToTheLastPosition() {
        rvGoaFeedDetailsList.post(new Runnable() {
            @Override
            public void run() {
                rvGoaFeedDetailsList.smoothScrollToPosition(mFirebaseAdapter.getItemCount());
            }
        });
    }

    private void initializeViews() {
        rvGoaFeedDetailsList = (RecyclerView) findViewById(R.id.rvGoaFeedDetailsList);
        etFeedBack = (EditText) findViewById(R.id.etFeedBack);
        btSend = (Button) findViewById(R.id.btSend);
        btSend.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btSend:
                String feedBackText = etFeedBack.getText().toString();
                feedBackText = feedBackText +"\n@" + LostInGoaApplication.getFromSharedPreference(this, Utils.USER_HANDLE_KEY);
                String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                long timeStamp = System.currentTimeMillis();
                if(feedBackText.equals("")){
                    Toast.makeText(this,"Please write Something",Toast.LENGTH_LONG).show();
                }else {
                    GoaFeedModel model = new GoaFeedModel();
                    model.setMessage(feedBackText);
                    model.setDateTime(currentDateTimeString);
                    model.setTimeStamp(timeStamp);
                    uploadFeedBackToServer(model);
                }
                break;
        }
    }

    private void uploadFeedBackToServer(GoaFeedModel goaFeed) {
        if(LostInGoaApplication.isInternetConnected(this)){
            String key = mGoaFeedReference.push().getKey();
            mGoaFeedReference.child(key).setValue(goaFeed);
            etFeedBack.setText("");
            scrollToTheLastPosition();
            //Toast.makeText(this,"Sent",Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(this,"No Internet Connection",Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.goa_feed_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.handle) {
            Intent intent = new Intent(this, UserHandleForFeed.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
