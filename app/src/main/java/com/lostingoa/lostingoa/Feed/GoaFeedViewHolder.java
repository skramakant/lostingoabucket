package com.lostingoa.lostingoa.Feed;

import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.View;
import android.widget.TextView;

import com.lostingoa.lostingoa.R;

/**
 * Created by ramakant on 15/10/16.
 */
public class GoaFeedViewHolder extends RecyclerView.ViewHolder {

    private TextView tvFeedMessage;
    private TextView tvFeedTime;
    public GoaFeedViewHolder(View itemView) {
        super(itemView);
        tvFeedMessage = (TextView) itemView.findViewById(R.id.tvFeedMessage);
        tvFeedTime = (TextView) itemView.findViewById(R.id.tvFeedTime);
    }

    public void bindEmergency(GoaFeedModel model) {
        tvFeedMessage.setText(model.getMessage());
        tvFeedTime.setText(model.getDateTime());
    }
}
