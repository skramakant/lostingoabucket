package com.lostingoa.lostingoa.Feed;

import java.util.Map;

/**
 * Created by ramakant on 15/10/16.
 */
public class GoaFeedModel {

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    private String message;
    private String dateTime;

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    private long timeStamp;

    public GoaFeedModel(){}

    public GoaFeedModel(String message, String dateTime){
        this.message = message;
        this.dateTime = dateTime;
    }
}
