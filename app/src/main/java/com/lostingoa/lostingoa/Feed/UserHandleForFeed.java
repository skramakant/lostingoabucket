package com.lostingoa.lostingoa.Feed;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.lostingoa.lostingoa.LostInGoaApplication;
import com.lostingoa.lostingoa.R;
import com.lostingoa.lostingoa.Utils;

public class UserHandleForFeed extends AppCompatActivity {

    private EditText etHandleName;
    private Button btOk;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_handle_for_feed);
        initializeViews();
    }

    private void initializeViews() {
        context = this;
        etHandleName = (EditText) findViewById(R.id.etHandleName);
        btOk = (Button) findViewById(R.id.btOk);
        btOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etHandleName.getText().toString().toLowerCase().equals("")){
                    return;
                }
                else if(etHandleName.getText().toString().toLowerCase().equals("lostingoa")){
                    Toast.makeText(UserHandleForFeed.this, "Lostingoa is a private handle!", Toast.LENGTH_SHORT).show();
                }else {
                    LostInGoaApplication.saveInSharedPreference(context, Utils.USER_HANDLE_KEY,etHandleName.getText().toString());
                    finish();
                }
            }
        });
    }
}
