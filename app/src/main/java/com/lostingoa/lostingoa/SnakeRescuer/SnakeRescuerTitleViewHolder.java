package com.lostingoa.lostingoa.SnakeRescuer;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.lostingoa.lostingoa.Database.DatabaseInstance;
import com.lostingoa.lostingoa.Database.DatabaseUtils;
import com.lostingoa.lostingoa.EmergencyModule.EmergencyDetails;
import com.lostingoa.lostingoa.EmergencyModule.EmergencyModel;
import com.lostingoa.lostingoa.R;

import java.util.ArrayList;

/**
 * Created by ramakant on 2/7/16.
 */
public class SnakeRescuerTitleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    View mView;
    Context mContext;

    public SnakeRescuerTitleViewHolder(View itemView) {
        super(itemView);
        mView = itemView;
        mContext = itemView.getContext();
        itemView.setOnClickListener(this);
    }

    public void bindEmergency(SnakeRescuerTitleModel snakeRescuerTitleModel){
        TextView tvName = (TextView) mView.findViewById(R.id.tvSnakeRescuer_title);
        tvName.setText(snakeRescuerTitleModel.getTitle());
    }

    @Override
    public void onClick(final View view) {
        final ArrayList<String> list = new ArrayList<>();
        DatabaseReference ref = DatabaseInstance.getFirebaseInstance().getReference(DatabaseUtils.SNAKE_RESCUER_TABLE_NAME);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //HashMap<String,EmergencyModel> map;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    //map = new HashMap<>();
                    //map.put(snapshot.getKey(),snapshot.getValue(EmergencyModel.class));
                    list.add((snapshot.getValue(EmergencyModel.class).getTitle()));
                    //map = null;
                }
                int itemPosition = getLayoutPosition();
                Intent intent = new Intent(mContext, SnakeRescuerTitleDetails.class);
                intent.putExtra("position", itemPosition+"");
                intent.putExtra("list", list);
                mContext.startActivity(intent);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(mContext,"error",Toast.LENGTH_LONG).show();
            }
        });
    }
}
