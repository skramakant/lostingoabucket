package com.lostingoa.lostingoa.SnakeRescuer;

/**
 * Created by ramakant on 2/7/16.
 */
public class SnakeRescuerTitleModel {

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    private String Title;

    public SnakeRescuerTitleModel(){}

    public SnakeRescuerTitleModel(String title){
        this.Title = title;
    }
}
