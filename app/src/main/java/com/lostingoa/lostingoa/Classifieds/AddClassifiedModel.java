package com.lostingoa.lostingoa.Classifieds;

/**
 * Created by ramakant on 19/2/17.
 */
public class AddClassifiedModel {

    public long getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(long startingTime) {
        this.startingTime = startingTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }

    public String getSelectedCat() {
        return selectedCat;
    }

    public void setSelectedCat(String selectedCat) {
        this.selectedCat = selectedCat;
    }

    public String getGetSelectedSubCat() {
        return getSelectedSubCat;
    }

    public void setGetSelectedSubCat(String getSelectedSubCat) {
        this.getSelectedSubCat = getSelectedSubCat;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getIsHighLighted() {
        return isHighLighted;
    }

    public void setIsHighLighted(boolean isHighLighted) {
        this.isHighLighted = isHighLighted;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonMobile() {
        return personMobile;
    }

    public void setPersonMobile(String personMobile) {
        this.personMobile = personMobile;
    }


    private long startingTime;
    private long endTime;
    private long addTime;
    private String selectedCat;
    private String getSelectedSubCat;
    private String description;
    private boolean isHighLighted;
    private String personName;
    private String personMobile;
    private String personEmail;


    public String getPersonEmail() {
        return personEmail;
    }

    public void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }

    public String getAddAmount() {
        return addAmount;
    }

    public void setAddAmount(String addAmount) {
        this.addAmount = addAmount;
    }

    private String addAmount;
}
