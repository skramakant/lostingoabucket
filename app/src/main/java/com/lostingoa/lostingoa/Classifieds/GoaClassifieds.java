package com.lostingoa.lostingoa.Classifieds;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.lostingoa.lostingoa.R;

public class GoaClassifieds extends AppCompatActivity implements OnItemClickListner {

    private RecyclerView recyclerView;
    private ClassifiedsCategeoryAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goa_classifieds);
        getSupportActionBar().setTitle("Classifieds");
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        setAdapter();
    }

    private void setAdapter() {
        adapter = new ClassifiedsCategeoryAdapter(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(View view, int position, String s) {
        Intent intent = new Intent(this,ClassifiedsSubCategeory.class);
        intent.putExtra("CAT_NAME",s);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_classified_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.add) {
            //return true;
            Intent intent = new Intent(this,AddClassifieds.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
