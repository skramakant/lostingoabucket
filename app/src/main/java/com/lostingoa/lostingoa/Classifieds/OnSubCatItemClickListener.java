package com.lostingoa.lostingoa.Classifieds;

import android.view.View;

/**
 * Created by ramakant on 21/2/17.
 */
public interface OnSubCatItemClickListener {

    void onSubCatItemClick(View view, int position, String s);
}
