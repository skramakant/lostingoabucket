package com.lostingoa.lostingoa.Classifieds;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.lostingoa.lostingoa.R;

public class ClassifiedsSubCategeory extends AppCompatActivity implements OnSubCatItemClickListener {
    private RecyclerView recyclerView;
    private ClassifiedsSubCategeoryAdapter adapter;
    String sunCategeory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classifieds_sub_categeory);
        if(getIntent().getExtras()  != null){
            sunCategeory = (String) getIntent().getExtras().get("CAT_NAME");
            getSupportActionBar().setTitle(sunCategeory);
        }
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        setAdapter();
    }

    private void setAdapter() {
        adapter = new ClassifiedsSubCategeoryAdapter(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onSubCatItemClick(View view, int position, String place) {
        Intent intent = new Intent(this,DisplayClassifiedsData.class);
        intent.putExtra("CAT_NAME",sunCategeory);
        intent.putExtra("SUB_CAT_NAME",place);
        startActivity(intent);
    }
}
