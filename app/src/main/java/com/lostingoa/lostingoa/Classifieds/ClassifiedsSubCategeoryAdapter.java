package com.lostingoa.lostingoa.Classifieds;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lostingoa.lostingoa.R;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ramakant on 18/2/17.
 */
public class ClassifiedsSubCategeoryAdapter extends RecyclerView.Adapter<ClassifiedsSubCategeoryAdapter.ClassifiedsSubCategeoryViewHolder> {
    private List<String> list = Arrays.asList("PANJIM","MARGOA","ARAMBOL","VAGATOR","ANJUNA",
            "BAGA","CALANGUTE","PALOLEM","GOA");
    private OnSubCatItemClickListener onSubCatItemClick;

    public ClassifiedsSubCategeoryAdapter(ClassifiedsSubCategeory classifiedsSubCategeory) {
        onSubCatItemClick = classifiedsSubCategeory;
    }

    @Override
    public ClassifiedsSubCategeoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.emergency_list_title,parent,false);
        return new ClassifiedsSubCategeoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ClassifiedsSubCategeoryViewHolder holder, final int position) {
        holder.title.setText(list.get(position));
        holder.lLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSubCatItemClick.onSubCatItemClick(view,position,list.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public static class ClassifiedsSubCategeoryViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private LinearLayout lLayout;
        public ClassifiedsSubCategeoryViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.em_title);
            lLayout = (LinearLayout)itemView.findViewById(R.id.lLayout);
        }
    }
}
