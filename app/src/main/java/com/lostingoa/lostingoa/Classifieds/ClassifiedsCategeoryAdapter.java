package com.lostingoa.lostingoa.Classifieds;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lostingoa.lostingoa.R;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ramakant on 18/2/17.
 */
public class ClassifiedsCategeoryAdapter extends RecyclerView.Adapter<ClassifiedsCategeoryAdapter.ClassifiedsCategeoryViewHolder> {

    private List<String> list = Arrays.asList("JOBS","ACCOMMODATION","E-STATE SELL","E-STATE BUY","BUSINESS OFFER",
            "EDUCATIONAL","PERSONAL","MATRIMONIAL","OTHERS");
    private OnItemClickListner onItemClickListner;

    public ClassifiedsCategeoryAdapter(GoaClassifieds goaClassifieds){
        onItemClickListner =  goaClassifieds;
    }

    @Override
    public ClassifiedsCategeoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.emergency_list_title,parent,false);
        return new ClassifiedsCategeoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ClassifiedsCategeoryViewHolder holder, final int position) {
        holder.title.setText(list.get(position));
        holder.lLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("test", "onClick: ");
                onItemClickListner.onItemClick(view,position,list.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ClassifiedsCategeoryViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        LinearLayout lLayout;
        public ClassifiedsCategeoryViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.em_title);
            lLayout = (LinearLayout) itemView.findViewById(R.id.lLayout);
        }
    }
}
