package com.lostingoa.lostingoa.Classifieds;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lostingoa.lostingoa.LostInGoaApplication;
import com.lostingoa.lostingoa.R;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by ramakant on 21/2/17.
 */
public class DisplayClassifiedViewHolder extends RecyclerView.ViewHolder {
    Context mContext;
    private TextView tvPersonName;
    private TextView tvDateTime;
    private TextView tvMobile;
    private ImageView iv_call;
    private TextView tvDetails;
    private RelativeLayout rlAddDetails;
    public DisplayClassifiedViewHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();
        tvPersonName = (TextView)itemView.findViewById(R.id.tvPersonName);
        tvDateTime = (TextView)itemView.findViewById(R.id.tvDateTime);
        tvMobile = (TextView)itemView.findViewById(R.id.tvMobile);
        tvDetails = (TextView)itemView.findViewById(R.id.tvDetails);
        iv_call = (ImageView)itemView.findViewById(R.id.iv_call);
        rlAddDetails = (RelativeLayout)itemView.findViewById(R.id.rlAddDetails);
    }

    public void bindClassifiedData(AddClassifiedModel model) {
        tvPersonName.setText(model.getPersonName());
        tvDateTime.setText(convertToDate(model.getStartingTime()));
        tvMobile.setText(model.getPersonMobile());
        tvDetails.setText(model.getDescription());
        if(model.getIsHighLighted()){
            tvDetails.setTextColor(Color.BLUE);
        }else {
            tvDetails.setTextColor(Color.BLACK);
        }
        iv_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LostInGoaApplication.phoneCall(mContext,tvMobile.getText().toString().trim());
            }
        });
    }

    public String convertToDate(long milis){
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date(milis));
        return currentDateTimeString;
    }


}
