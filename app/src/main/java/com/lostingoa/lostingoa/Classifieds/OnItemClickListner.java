package com.lostingoa.lostingoa.Classifieds;

import android.view.View;

/**
 * Created by ramakant on 18/2/17.
 */
public interface OnItemClickListner {

    void onItemClick(View view, int position, String s);
}
