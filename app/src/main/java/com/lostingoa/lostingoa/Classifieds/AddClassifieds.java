package com.lostingoa.lostingoa.Classifieds;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.util.TimeUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.instamojo.android.activities.PaymentDetailsActivity;
import com.instamojo.android.callbacks.OrderRequestCallBack;
import com.instamojo.android.helpers.Constants;
import com.instamojo.android.models.Errors;
import com.instamojo.android.models.Order;
import com.lostingoa.lostingoa.Database.DatabaseInstance;
import com.lostingoa.lostingoa.Database.DatabaseUtils;
import com.lostingoa.lostingoa.LostInGoaApplication;
import com.lostingoa.lostingoa.R;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Text;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddClassifieds extends AppCompatActivity {
    private static int BASE_COST = 20;
    private static int BASE_HLIGHT_COST = 20;
    private Spinner catSpinner;
    private Spinner subCatSpinner;
    private static TextView tvStartDate;
    private static TextView tvEndDate;
    private static TextView tvNDay;
    private static TextView tvNDayError;
    private static TextView tvCost;
    private TextView tvHighlighted;
    private EditText etPersonName;
    private EditText etPersonMobile;
    private EditText etPersonEmail;
    public TextInputEditText addContent;
    private static AddClassifiedModel addClassifiedData;
    private Button postAddButton;
    private static  boolean startDateFlag = false;
    private static boolean endDateFlag = false;
    private static int totalCost = 0;

    //Database
    private DatabaseReference mDatabase;
    private DatabaseReference mGoaClassifiedsReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_classifieds);
        addClassifiedData = new AddClassifiedModel();
        mDatabase = DatabaseInstance.getFirebaseInstance().getReference();
        mGoaClassifiedsReference = DatabaseInstance.getFirebaseInstance().getReference().child(DatabaseUtils.GOA_CLASSIFIEDS_TABLE_NAME);

        initializeViews();
    }

    private void initializeViews() {
        catSpinner = (Spinner) findViewById(R.id.catSpinner);
        subCatSpinner = (Spinner) findViewById(R.id.subCatSpinner);
        tvStartDate = (TextView) findViewById(R.id.tvStartDate);
        tvEndDate = (TextView) findViewById(R.id.tvEndDate);
        addContent = (TextInputEditText) findViewById(R.id.addContent);
        tvHighlighted = (TextView) findViewById(R.id.tvHighlighted);
        postAddButton = (Button) findViewById(R.id.postAddButton);
        etPersonName = (EditText) findViewById(R.id.etPersonName);
        etPersonMobile = (EditText) findViewById(R.id.etPersonMobile);
        etPersonEmail = (EditText) findViewById(R.id.etPersonEmail);
        tvNDay = (TextView) findViewById(R.id.tvNDay);
        tvNDayError = (TextView) findViewById(R.id.tvNDayError);
        tvCost = (TextView)findViewById(R.id.tvCost);
        setUpSpinner();
    }

    public void postAddOnServer(View view){
        String addData = addContent.getText().toString();
        if(addData != null && !addData.equals("")){

            long currentTime = System.currentTimeMillis();
            //long startTime = addClassifiedData.getStartingTime();
            //long sDiff = currentTime-startTime;
            //startTime = startTime + sDiff;//adding time in date
            //long endTime = addClassifiedData.getEndTime();
            //endTime = endTime + sDiff;
            addClassifiedData.setAddTime(currentTime);
            //addClassifiedData.setStartingTime(startTime);
            //addClassifiedData.setEndTime(endTime);

            //long duration = endTime-startTime;
            long duration = addClassifiedData.getEndTime() - addClassifiedData.getStartingTime();
            long diffInHours = TimeUnit.MILLISECONDS.toHours(duration);

            if(diffInHours < 24*2 || diffInHours>24*7 ){
                Toast.makeText(this,"mini. add duration 2days and max. 7 days",Toast.LENGTH_LONG).show();
                Log.v("abd","mini add duration 2days and max 7 days ");
                return;
            }else {
                addClassifiedData.setDescription(addData);

                String personName = etPersonName.getText().toString();
                if(personName != null && personName.equals("")){
                    Toast.makeText(AddClassifieds.this, "Please Enter Your Name", Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    addClassifiedData.setPersonName(personName);
                }

                String personMobile = etPersonMobile.getText().toString();
                if(personMobile !=null && personMobile.equals("")){
                    Toast.makeText(AddClassifieds.this, "Please Enter Mobile Number", Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    addClassifiedData.setPersonMobile(personMobile);
                }

                String personEmail = etPersonEmail.getText().toString();
                if(personEmail != null && personEmail.equals("")){
                    Toast.makeText(AddClassifieds.this, "Please Enter Email Id", Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    addClassifiedData.setPersonEmail(personEmail);
                }

                addClassifiedData.setAddAmount(Integer.toString(totalCost));

                LostInGoaApplication.showProgressDialog(this);
                getAccessTokenForPayment(addClassifiedData);
                //uploadAddToServer(addClassifiedData);
            }
        }else {
            Toast.makeText(AddClassifieds.this, "Please enter Add", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    private void uploadAddToServer(AddClassifiedModel addClassifiedData) {
        String cat = addClassifiedData.getSelectedCat().toUpperCase();
        String sub_cat = addClassifiedData.getGetSelectedSubCat().toUpperCase();
        String key = mDatabase.child(DatabaseUtils.GOA_CLASSIFIEDS_TABLE_NAME).child(cat).child(sub_cat).push().getKey();
        mDatabase.child(DatabaseUtils.GOA_CLASSIFIEDS_TABLE_NAME).child(cat).child(sub_cat).child(key).setValue(addClassifiedData).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                LostInGoaApplication.hideProgressDialog();
                Toast.makeText(getApplicationContext(),"Add successfully Posted",Toast.LENGTH_LONG).show();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                });
            }
        });
    }

    public void payAddCost(View view){
        //Toast.makeText(this,"Payment Successful",Toast.LENGTH_LONG).show();
        //getAccessTokenForPayment();
    }

    private void getAccessTokenForPayment(final AddClassifiedModel addClassifiedData) {
        OkHttpClient client = new OkHttpClient();

//        RequestBody requestBody = new MultipartBody.Builder()
//                .setType(MultipartBody.FORM)
//                .addFormDataPart("client_id", "PqTyXG90QCVYfHLdsNo7eyLv5ywzd7D8aym5IQNI")
//                .addFormDataPart("client_secret", "2BEkvhw5YK39KMCGkiCJjFlnkboBr13vGPdB9HO14AfUufa3wHfV0CwRTSVNLZ3lW8BMTgGrDRfEzxcF8IsIcdc0L3Gjie7q7J9824BquUfkqeegzfPQOeddyOoiCYD5")
//                .addFormDataPart("grant_type", "client_credentials")
//                .build();
//
//       Request request = new Request.Builder()
//                .url("https://api.instamojo.com/oauth2/token/")
//                .method("POST", RequestBody.create(null, new byte[0]))
//                .post(requestBody)
//                .header("Content-Type","application/x-www-form-urlencoded")
//                .build();

        RequestBody formBody = new FormBody.Builder()
                .add("client_id", "PqTyXG90QCVYfHLdsNo7eyLv5ywzd7D8aym5IQNI")
                .add("client_secret", "2BEkvhw5YK39KMCGkiCJjFlnkboBr13vGPdB9HO14AfUufa3wHfV0CwRTSVNLZ3lW8BMTgGrDRfEzxcF8IsIcdc0L3Gjie7q7J9824BquUfkqeegzfPQOeddyOoiCYD5")
                .add("grant_type", "client_credentials")
                .build();
        final Request request = new Request.Builder()
                .header("Content-Type","application/x-www-form-urlencoded")
                .url("https://api.instamojo.com/oauth2/token/")
                .post(formBody)
                .build();

        Call call = client.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("HttpService", "onFailure() Request was: " );

                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String res = response.body().string();

                Log.e("response ", "onResponse(): " + response );
                JSONParser parser = new JSONParser();
                try {
                    org.json.simple.JSONObject object = (org.json.simple.JSONObject) parser.parse(res);
                    String access_token = (String) object.get("access_token");
                    Random rand = new Random();
                    String transactionId = Integer.toString(rand.nextInt(10000)+1);
                    Log.v("token",access_token);
                    createOrder(access_token, transactionId,addClassifiedData);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void createOrder(String accessToken, String transactionID,AddClassifiedModel addClassifiedData) {
        String name = addClassifiedData.getPersonName();
        final String email = addClassifiedData.getPersonEmail();
        String phone = addClassifiedData.getPersonMobile();
        String amount = addClassifiedData.getAddAmount();
        String description = "I am paying amount of "+amount+"Rs for posting advertisement on lostingoa";//descriptionBox.getText().toString();

        //Create the Order
        Order order = new Order(accessToken, transactionID, name, email, phone, amount, description);
        //transactionID
        //set webhook
        //order.setWebhook("http://your.server.com/webhook/");

        //Validate the Order
        if (!order.isValid()) {
            //oops order validation failed. Pinpoint the issue(s).

            if (!order.isValidName()) {
                //nameBox.setError("Buyer name is invalid");
            }

            if (!order.isValidEmail()) {
                //emailBox.setError("Buyer email is invalid");
            }

            if (!order.isValidPhone()) {
                //phoneBox.setError("Buyer phone is invalid");
            }

            if (!order.isValidAmount()) {
                //amountBox.setError("Amount is invalid or has more than two decimal places");
            }

            if (!order.isValidDescription()) {
                //descriptionBox.setError("Description is invalid");
            }

            if (!order.isValidTransactionID()) {
                showToast("Transaction is Invalid");
            }

            if (!order.isValidRedirectURL()) {
                showToast("Redirection URL is invalid");
            }

            if (!order.isValidWebhook()) {
                showToast("Webhook URL is invalid");
            }

            return;
        }

        //Validation is successful. Proceed
        //dialog.show();
        com.instamojo.android.network.Request request = new com.instamojo.android.network.Request(order, new OrderRequestCallBack() {
            @Override
            public void onFinish(final Order order, final Exception error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //dialog.dismiss();
                        if (error != null) {
                            if (error instanceof Errors.ConnectionError) {
                                showToast("No internet connection");
                            } else if (error instanceof Errors.ServerError) {
                                showToast("Server Error. Try again");
                            } else if (error instanceof Errors.AuthenticationError) {
                                showToast("Access token is invalid or expired. Please Update the token!!");
                            } else if (error instanceof Errors.ValidationError) {
                                // Cast object to validation to pinpoint the issue
                                Errors.ValidationError validationError = (Errors.ValidationError) error;

                                if (!validationError.isValidTransactionID()) {
                                    showToast("Transaction ID is not Unique");
                                    return;
                                }

                                if (!validationError.isValidRedirectURL()) {
                                    showToast("Redirect url is invalid");
                                    return;
                                }

                                if (!validationError.isValidWebhook()) {
                                    showToast("Webhook url is invalid");
                                    return;
                                }

                                if (!validationError.isValidPhone()) {
                                    //phoneBox.setError("Buyer's Phone Number is invalid/empty");
                                    return;
                                }

                                if (!validationError.isValidEmail()) {
                                    //emailBox.setError("Buyer's Email is invalid/empty");
                                    return;
                                }

                                if (!validationError.isValidAmount()) {
                                    //amountBox.setError("Amount is either less than Rs.9 or has more than two decimal places");
                                    return;
                                }

                                if (!validationError.isValidName()) {
                                    //nameBox.setError("Buyer's Name is required");
                                    return;
                                }
                            } else {
                                showToast(error.getMessage());
                            }
                            return;
                        }

                        startPreCreatedUI(order);
                    }
                });
            }
        });

        request.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CODE && data != null) {
            String orderID = data.getStringExtra(Constants.ORDER_ID);
            String transactionID = data.getStringExtra(Constants.TRANSACTION_ID);
            String paymentID = data.getStringExtra(Constants.PAYMENT_ID);

            // Check transactionID, orderID, and orderID for null before using them to check the Payment status.
            if (transactionID != null || paymentID != null || orderID!=null) {
                //checkPaymentStatus(transactionID, orderID);
                showToast("Great! Payment is successful");
                LostInGoaApplication.showProgressDialog(this);
                uploadAddToServer(addClassifiedData);
            } else {
                showToast("Oops!! Payment was cancelled");
            }
        }
    }

//    private HttpUrl.Builder getHttpURLBuilder() {
//        return new HttpUrl.Builder()
//                .scheme("https")
//                .host("sample-sdk-server.instamojo.com");
//    }
//
//    /**
//     * Will check for the transaction status of a particular Transaction
//     *
//     * @param transactionID Unique identifier of a transaction ID
//     */
//    private void checkPaymentStatus(final String transactionID, final String orderID) {
//        if (accessToken == null || (transactionID == null && orderID == null)) {
//            return;
//        }
//
//        if (dialog != null && !dialog.isShowing()) {
//            dialog.show();
//        }
//
//        showToast("checking transaction status");
//        OkHttpClient client = new OkHttpClient();
//        HttpUrl.Builder builder = getHttpURLBuilder();
//        builder.addPathSegment("status");
//        if (transactionID != null){
//            builder.addQueryParameter("transaction_id", transactionID);
//        } else {
//            builder.addQueryParameter("id", orderID);
//        }
//        builder.addQueryParameter("env", currentEnv.toLowerCase());
//        HttpUrl url = builder.build();
//
//        okhttp3.Request request = new okhttp3.Request.Builder()
//                .url(url)
//                .addHeader("Authorization", "Bearer " + accessToken)
//                .build();
//        client.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (dialog != null && dialog.isShowing()) {
//                            dialog.dismiss();
//                        }
//                        showToast("Failed to fetch the Transaction status");
//                    }
//                });
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                String responseString = response.body().string();
//                response.body().close();
//                String status = null;
//                String paymentID = null;
//                String amount = null;
//                String errorMessage = null;
//
//                try {
//                    JSONObject responseObject = new JSONObject(responseString);
//                    JSONObject payment = responseObject.getJSONArray("payments").getJSONObject(0);
//                    status = payment.getString("status");
//                    paymentID = payment.getString("id");
//                    amount = responseObject.getString("amount");
//
//                } catch (JSONException e) {
//                    errorMessage = "Failed to fetch the Transaction status";
//                }
//
//                final String finalStatus = status;
//                final String finalErrorMessage = errorMessage;
//                final String finalPaymentID = paymentID;
//                final String finalAmount = amount;
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (dialog != null && dialog.isShowing()) {
//                            dialog.dismiss();
//                        }
//                        if (finalStatus == null) {
//                            showToast(finalErrorMessage);
//                            return;
//                        }
//
//                        if (!finalStatus.equalsIgnoreCase("successful")) {
//                            showToast("Transaction still pending");
//                            return;
//                        }
//
//                        showToast("Transaction Successful for id - " + finalPaymentID);
//                        //refundTheAmount(transactionID, finalAmount);
//                    }
//                });
//            }
//        });
//
//    }
//
//    /**
//     * Will initiate a refund for a given transaction with given amount
//     *
//     * @param transactionID Unique identifier for the transaction
//     * @param amount    amount to be refunded
//     */
//    private void refundTheAmount(String transactionID, String amount) {
//        if (accessToken == null || transactionID == null || amount == null) {
//            return;
//        }
//
//        if (dialog != null && !dialog.isShowing()) {
//            dialog.show();
//        }
//
//        showToast("Initiating a refund for - " + amount);
//        OkHttpClient client = new OkHttpClient();
//        HttpUrl url = getHttpURLBuilder()
//                .addPathSegment("refund")
//                .addPathSegment("")
//                .build();
//
//        RequestBody body = new FormBody.Builder()
//                .add("env", currentEnv.toLowerCase())
//                .add("transaction_id", transactionID)
//                .add("amount", amount)
//                .add("type", "PTH")
//                .add("body", "Refund the Amount")
//                .build();
//
//        okhttp3.Request request = new okhttp3.Request.Builder()
//                .url(url)
//                .addHeader("Authorization", "Bearer " + accessToken)
//                .post(body)
//                .build();
//
//        client.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (dialog != null && dialog.isShowing()) {
//                            dialog.dismiss();
//                        }
//                        LostInGoaApplication.showProgressDialog();
//                        showToast("Failed to Initiate a refund");
//                    }
//                });
//            }
//
//            @Override
//            public void onResponse(Call call, final Response response) throws IOException {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
////                        if (dialog != null && dialog.isShowing()) {
////                            dialog.dismiss();
////                        }
//                        LostInGoaApplication.hideProgressDialog();
//                        String message;
//
//                        if (response.isSuccessful()) {
//                            message = "Refund intiated successfully";
//                        } else {
//                            message = "Failed to Initiate a refund";
//                        }
//
//                        showToast(message);
//                    }
//                });
//            }
//        });
//    }

    private void showToast(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void startPreCreatedUI(Order order) {
        //Using Pre created UI
        LostInGoaApplication.hideProgressDialog();
        Intent intent = new Intent(getBaseContext(), PaymentDetailsActivity.class);
        intent.putExtra(Constants.ORDER, order);
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    private void setUpSpinner() {
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> catAdapter = ArrayAdapter.createFromResource(this,
                R.array.cat_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        catAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        catSpinner.setAdapter(catAdapter);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> subCatAdapter = ArrayAdapter.createFromResource(this,
                R.array.sub_cat_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        subCatAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        subCatSpinner.setAdapter(subCatAdapter);


        //setup onClick Listener for both spinner
        setupOnClickListenerForBothSpinner();
    }

    private void setupOnClickListenerForBothSpinner() {
        catSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedCat = (String) adapterView.getItemAtPosition(i);
                addClassifiedData.setSelectedCat(selectedCat);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        subCatSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedSubCat = (String) adapterView.getItemAtPosition(i);
                addClassifiedData.setGetSelectedSubCat(selectedSubCat);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void showStartDatePickerDialog(View v) {
        startDateFlag = true;
        endDateFlag = false;
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void showEndDatePickerDialog(View v) {
        startDateFlag = false;
        endDateFlag = true;
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis()-10000);
            return datePickerDialog;
        }


        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int date) {
            showDate(year,month,date);
        }
    }

    public static void showDate(int year, int month, int day) {
        if(startDateFlag){
//            tvStartDate.setText(new StringBuilder().append(day).append("/")
//                    .append(month).append("/").append(year));
            long dateMilliseconds = getDateMilliseconds(year,month,day);
            addClassifiedData.setStartingTime(dateMilliseconds);
            tvStartDate.setText(getCurrentTimeStamp(dateMilliseconds));
            startDateFlag = false;
        }else if(endDateFlag){
//            tvEndDate.setText(new StringBuilder().append(day).append("/")
//                    .append(month).append("/").append(year));
            long dateMilliseconds = getDateMilliseconds(year,month,day);
            addClassifiedData.setEndTime(dateMilliseconds);
            tvEndDate.setText(getCurrentTimeStamp(dateMilliseconds));
            endDateFlag = false;
        }
        calculateDayToShowAdd();
    }
    public static String getCurrentTimeStamp(long milliseconds) {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
        Date now = new Date(milliseconds);
        String strDate = sdfDate.format(now);
        return strDate;
    }
    private static void calculateDayToShowAdd() {
        long currentTime = System.currentTimeMillis();
        long startTime = addClassifiedData.getStartingTime();
        long sDiff = currentTime-startTime;
        startTime = startTime + sDiff;//adding time in date
        long endTime = addClassifiedData.getEndTime();
        endTime = endTime + sDiff;
        if(endTime > startTime){
            long duration = endTime-startTime;
            long diffDay = TimeUnit.MILLISECONDS.toDays(duration);
            if(diffDay >= 2 && diffDay <= 7){
                tvNDay.setText("Your Add Will Run For "+diffDay+" Days");
                tvNDay.setVisibility(View.VISIBLE);
                tvNDayError.setVisibility(View.GONE);
                calculateTotalCostForThisAdd(diffDay);
            }else {
                tvNDay.setVisibility(View.GONE);
                tvNDayError.setVisibility(View.VISIBLE);
                tvCost.setText("Total Cost");
            }
        }else {
            tvNDay.setVisibility(View.GONE);
            tvNDayError.setVisibility(View.VISIBLE);
            tvCost.setText("Total Cost");
        }
    }

    private static void calculateTotalCostForThisAdd(long diffDay) {
        int baseCost = BASE_COST;
        totalCost = 0;
        for(int i=0;i<diffDay;i++){
            totalCost = totalCost + (baseCost - i);
        }
        if(addClassifiedData.getIsHighLighted()){
            totalCost = totalCost + BASE_HLIGHT_COST;
        }
        tvCost.setText("Please Pay "+totalCost+"Rs");
    }

    public static long getDateMilliseconds(int year, int month, int day){
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        long time = calendar.getTimeInMillis();
        return time;
    }
    public void onHighlightedCheckboxClicked(View view){
        boolean checked = ((CheckBox) view).isChecked();
        switch (view.getId()){
            case R.id.isHighLighted:
                if(checked){
                    addClassifiedData.setIsHighLighted(true);
                    tvHighlighted.setText("It will charge 20rs more.");
                    calculateDayToShowAdd();
                }else {
                    tvHighlighted.setText("Want Highlighted Add?");
                    addClassifiedData.setIsHighLighted(false);
                    calculateDayToShowAdd();
                }
        }
    }
}
