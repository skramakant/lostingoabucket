package com.lostingoa.lostingoa.Classifieds;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.lostingoa.lostingoa.Database.DatabaseInstance;
import com.lostingoa.lostingoa.Database.DatabaseUtils;
import com.lostingoa.lostingoa.LostInGoaApplication;
import com.lostingoa.lostingoa.R;

import java.util.concurrent.TimeUnit;

public class DisplayClassifiedsData extends AppCompatActivity {
    private RecyclerView recyclerView;
    private DatabaseReference mGoaFeedReference;
    private FirebaseRecyclerAdapter mFirebaseAdapter;
    private String cat_name;
    private String sub_cat_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_classifieds_data);
        Bundle bundle = getIntent().getExtras();
        if(getIntent().getExtras() != null){
            cat_name = (String) bundle.get("CAT_NAME");
            sub_cat_name = (String) bundle.get("SUB_CAT_NAME");
        }
        if(sub_cat_name != null){
            getSupportActionBar().setTitle(sub_cat_name.toUpperCase());
        }else {
            getSupportActionBar().setTitle("Advertisements");
        }
        initializeView();
        mGoaFeedReference = DatabaseInstance.getFirebaseInstance().getReference().child(DatabaseUtils.GOA_CLASSIFIEDS_TABLE_NAME).child(cat_name).child(sub_cat_name);
        //deleteOlderAddFromFirebase();
        mGoaFeedReference.keepSynced(true);
        setUpFirebaseAdapter();
    }

    private void setUpFirebaseAdapter() {
        LostInGoaApplication.showProgressDialog(this);
        Query allDataUpToTodayDate = mGoaFeedReference.orderByChild("startingTime").endAt(System.currentTimeMillis());
        mFirebaseAdapter = new FirebaseRecyclerAdapter<AddClassifiedModel,DisplayClassifiedViewHolder>(AddClassifiedModel.class, R.layout.display_classified_data_item, DisplayClassifiedViewHolder.class,
                allDataUpToTodayDate) {

            @Override
            public void onBindViewHolder(DisplayClassifiedViewHolder viewHolder, int position) {
                super.onBindViewHolder(viewHolder, position);
                AddClassifiedModel model = getItem(position);
//                if(model.getStartingTime()>System.currentTimeMillis()){
//                    Log.v("Classified","do not show this add");
//
//                }else
                if(model.getEndTime()<System.currentTimeMillis()){
                    Log.v("Classified","delete expired add");
                    getRef(position).removeValue();
                }else{
                    Log.v("Classified","show add");
                    populateViewHolder(viewHolder, model, position);
                }
            }

            @Override
            protected void populateViewHolder(DisplayClassifiedViewHolder viewHolder, AddClassifiedModel model, int position) {
                viewHolder.bindClassifiedData(model);
            }
        };
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(mFirebaseAdapter);
        allDataUpToTodayDate.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                LostInGoaApplication.hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void initializeView() {
        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
    }

    /**
     * this message is only for ADMIN
     */
    private void deleteOlderMessage() {
        long cutoff = System.currentTimeMillis() - TimeUnit.MILLISECONDS.convert(12, TimeUnit.HOURS);
        Query oldItems = mGoaFeedReference.orderByChild("timeStamp").endAt(cutoff);
        oldItems.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot itemSnapshot: snapshot.getChildren()) {
                    itemSnapshot.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void deleteOlderAddFromFirebase() {
        mGoaFeedReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                AddClassifiedModel data = dataSnapshot.getValue(AddClassifiedModel.class);
                if(data.getEndTime()<= System.currentTimeMillis()){
                    dataSnapshot.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
