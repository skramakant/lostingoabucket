package com.lostingoa.lostingoa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.lostingoa.lostingoa.Banks.BanksTitle;
import com.lostingoa.lostingoa.Bike.BikeTitle;
import com.lostingoa.lostingoa.Classifieds.GoaClassifieds;
import com.lostingoa.lostingoa.EmergencyModule.Emergency;
import com.lostingoa.lostingoa.Entertainment.EntertainmentTitle;
import com.lostingoa.lostingoa.Feed.GoaFeed;
import com.lostingoa.lostingoa.FeedBack.FeedBack;
import com.lostingoa.lostingoa.Notification.NotificationActivity;
import com.lostingoa.lostingoa.PostOffice.PostOfficeTitle;
import com.lostingoa.lostingoa.PostOffice.PostOfficeTitleDetails;
import com.lostingoa.lostingoa.Repair.RepairTitle;
import com.lostingoa.lostingoa.SafetyPolice.SafetyPoliceTitleDetails;
import com.lostingoa.lostingoa.SnakeRescuer.SnakeRescuerList;
import com.lostingoa.lostingoa.Taxi.TaxiTitle;
import com.lostingoa.lostingoa.Transport.TransportTitle;

public class Dashboard extends AppCompatActivity implements View.OnClickListener {
    public static String TAG = Dashboard.class.getSimpleName();

    private LinearLayout llEmergency;
    private LinearLayout llSnakeRescuer;
    private LinearLayout llSafetyPolice;
    private LinearLayout llTransport;
    private LinearLayout llBanks;
    private LinearLayout llPostOffice;
    private LinearLayout llBus;
    private LinearLayout llTaxi;
    private LinearLayout llBike;
    private LinearLayout llEntertainment;
    private LinearLayout llRepair;
    private LinearLayout llClassifieds;
    private LinearLayout llFeedBack;
    private LinearLayout llGoaFeed;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        initializeViews();
        if(getIntent().getExtras() != null){
            Bundle notificationBundel = getIntent().getExtras().getBundle("NOTIFY");
            if(notificationBundel != null){
                Intent intent = new Intent(this, NotificationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("NOTIFY",notificationBundel);
                startActivity(intent);
            }
        }
    }

    private void initializeViews() {
        llEmergency = (LinearLayout) findViewById(R.id.llEmergency);
        llEmergency.setOnClickListener(this);
        llSnakeRescuer = (LinearLayout) findViewById(R.id.llSnakeRescuer);
        llSnakeRescuer.setOnClickListener(this);
        llSafetyPolice = (LinearLayout) findViewById(R.id.llSafetyPolice);
        llSafetyPolice.setOnClickListener(this);
        llTransport = (LinearLayout) findViewById(R.id.llTransport);
        llTransport.setOnClickListener(this);
        llBanks = (LinearLayout) findViewById(R.id.llBanks);
        llBanks.setOnClickListener(this);
        llPostOffice = (LinearLayout) findViewById(R.id.llPostOffice);
        llPostOffice.setOnClickListener(this);
        //llBus = (LinearLayout) findViewById(R.id.llBus);
        //llBus.setOnClickListener(this);
        //llTaxi = (LinearLayout) findViewById(R.id.llTaxi);
        //llTaxi.setOnClickListener(this);
        llBike = (LinearLayout) findViewById(R.id.llBike);
        llBike.setOnClickListener(this);
        llEntertainment = (LinearLayout) findViewById(R.id.llEntertainment);
        llEntertainment.setOnClickListener(this);
        llRepair = (LinearLayout) findViewById(R.id.llRepair);
        llRepair.setOnClickListener(this);
        llClassifieds = (LinearLayout) findViewById(R.id.llClassifieds);
        llClassifieds.setOnClickListener(this);
        llFeedBack = (LinearLayout) findViewById(R.id.llFeedBack);
        llFeedBack.setOnClickListener(this);
        llGoaFeed = (LinearLayout) findViewById(R.id.llGoaFeed);
        llGoaFeed.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.llEmergency:
                intent = new Intent(this, Emergency.class);
                startActivity(intent);
                break;
            case R.id.llSnakeRescuer:
                intent = new Intent(this, SnakeRescuerList.class);
                startActivity(intent);
                break;
            case R.id.llSafetyPolice:
                intent = new Intent(this, SafetyPoliceTitleDetails.class);
                startActivity(intent);
                break;
            case R.id.llTransport:
                intent = new Intent(this, TransportTitle.class);
                startActivity(intent);
                break;
            case R.id.llBanks:
                intent = new Intent(this, BanksTitle.class);
                startActivity(intent);
                break;
            case R.id.llPostOffice:
                intent = new Intent(this, PostOfficeTitleDetails.class);
                startActivity(intent);
                break;
/*            case R.id.llBus:
                break;
            case R.id.llTaxi:
                intent = new Intent(this, TaxiTitle.class);
                startActivity(intent);
                break;*/
            case R.id.llBike:
                intent = new Intent(this, BikeTitle.class);
                startActivity(intent);
                break;
            case R.id.llEntertainment:
                intent = new Intent(this, EntertainmentTitle.class);
                startActivity(intent);
                break;
            case R.id.llRepair:
                intent = new Intent(this, RepairTitle.class);
                startActivity(intent);
                break;
            case R.id.llFeedBack:
                intent = new Intent(this, NotificationActivity.class);
                startActivity(intent);
                break;
            case R.id.llGoaFeed:
                intent = new Intent(this, GoaFeed.class);
                startActivity(intent);
                break;
            case R.id.llClassifieds:
                intent = new Intent(this, GoaClassifieds.class);
                startActivity(intent);
                break;

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.map) {
            Intent intent = new Intent(this, MapView.class);
            intent.putExtra("map_key","goa_tourist_map");
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.entertainment_map_menu, menu);
        return true;
    }
}
