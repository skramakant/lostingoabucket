package com.lostingoa.lostingoa.Entertainment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.lostingoa.lostingoa.Database.DatabaseInstance;
import com.lostingoa.lostingoa.Database.DatabaseUtils;
import com.lostingoa.lostingoa.EmergencyModule.AddEmergencyTitle;
import com.lostingoa.lostingoa.EmergencyModule.EmergencyModel;
import com.lostingoa.lostingoa.EmergencyModule.EmergencyViewHolder;
import com.lostingoa.lostingoa.LostInGoaApplication;
import com.lostingoa.lostingoa.R;
import com.lostingoa.lostingoa.Utils;

public class EntertainmentTitle extends AppCompatActivity {
    private RecyclerView emList;
    private DatabaseReference mEmergencyReference;
    private FirebaseRecyclerAdapter mFirebaseAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entertainment_title);
        getSupportActionBar().setTitle("Entertainment");
        initializeViews();
        mEmergencyReference = DatabaseInstance.getFirebaseInstance().getReference(DatabaseUtils.ENTERTAINMENT_TABLE_NAME);
        mEmergencyReference.keepSynced(true);
        setUpFirebaseAdapter();
    }

    private void setUpFirebaseAdapter() {
        LostInGoaApplication.showProgressDialog(this);
        mFirebaseAdapter = new FirebaseRecyclerAdapter<EmergencyModel,EmergencyViewHolder>(EmergencyModel.class, R.layout.emergency_list_title, EmergencyViewHolder.class,
                mEmergencyReference) {

            @Override
            protected void populateViewHolder(EmergencyViewHolder viewHolder, EmergencyModel model, int position) {
                viewHolder.bindEmergency(model, Utils.ADD_ENTERTAINMENT_TITLE);
            }
        };
        emList.setHasFixedSize(true);
        emList.setLayoutManager(new LinearLayoutManager(this));
        emList.setAdapter(mFirebaseAdapter);
        mEmergencyReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                LostInGoaApplication.hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void initializeViews() {
        emList = (RecyclerView) findViewById(R.id.llEntertainmentList);
    }

/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.emergency_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.add) {
            //return true;
            Intent intent = new Intent(this,AddEmergencyTitle.class);
            intent.putExtra(Utils.KEY_ADD_TITLE,Utils.ADD_ENTERTAINMENT_TITLE);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }*/
}
