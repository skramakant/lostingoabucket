package com.lostingoa.lostingoa.Bike;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.lostingoa.lostingoa.Database.DatabaseInstance;
import com.lostingoa.lostingoa.Database.DatabaseUtils;
import com.lostingoa.lostingoa.EmergencyModule.AddEmergencyTitle;
import com.lostingoa.lostingoa.EmergencyModule.EmergencyModel;
import com.lostingoa.lostingoa.EmergencyModule.EmergencyViewHolder;
import com.lostingoa.lostingoa.LostInGoaApplication;
import com.lostingoa.lostingoa.R;
import com.lostingoa.lostingoa.Utils;

public class BikeTitle extends AppCompatActivity {
    private RecyclerView emList;
    private ImageView ivCat;
    private DatabaseReference mEmergencyReference;
    private FirebaseRecyclerAdapter mFirebaseAdapter;
    private FirebaseStorage storage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bike_title);
        //getSupportActionBar().setTitle("Bike");
        initializeViews();
        setCatImage();
        mEmergencyReference = DatabaseInstance.getFirebaseInstance().getReference(DatabaseUtils.BIKE_PILOT_TABLE_NAME);
        mEmergencyReference.keepSynced(true);
        setUpFirebaseAdapter();
    }

    private void setUpFirebaseAdapter() {
        LostInGoaApplication.showProgressDialog(this);
        mFirebaseAdapter = new FirebaseRecyclerAdapter<EmergencyModel,EmergencyViewHolder>(EmergencyModel.class, R.layout.emergency_list_title, EmergencyViewHolder.class,
                mEmergencyReference) {

            @Override
            protected void populateViewHolder(EmergencyViewHolder viewHolder, EmergencyModel model, int position) {
                viewHolder.bindEmergency(model, Utils.ADD_BIKE_TITLE);
            }
        };
        emList.setHasFixedSize(true);
        emList.setLayoutManager(new LinearLayoutManager(this));
        emList.setAdapter(mFirebaseAdapter);
        mEmergencyReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                LostInGoaApplication.hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void initializeViews() {
        ivCat = (ImageView) findViewById(R.id.ivCat);
        emList = (RecyclerView) findViewById(R.id.llBikeList);
    }

    private void setCatImage() {
        storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference();
        StorageReference pathReference = storageRef.child("lostingoa/catimages/scooter.png");
        StorageReference httpsReference = storage.getReferenceFromUrl("https://firebasestorage.googleapis.com/v0/b/lostingoa-cac5a.appspot.com/o/lostingoa%2Fcatimages%2Fscooter.png?alt=media&token=4426bc80-183d-4076-90c2-682aa323a40a");
//        httpsReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
//            @Override
//            public void onSuccess(Uri uri) {
//                ivCat.setImageURI(null);
//                ivCat.setImageURI(uri);
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//
//            }
//        });
        Glide.with(this)
                .using(new FirebaseImageLoader())
                .load(pathReference)
                .into(ivCat);
    }

/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.emergency_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.add) {
            //return true;
            Intent intent = new Intent(this,AddEmergencyTitle.class);
            intent.putExtra(Utils.KEY_ADD_TITLE,Utils.ADD_BIKE_TITLE);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }*/
}
