package com.lostingoa.lostingoa.EmergencyModule;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.lostingoa.lostingoa.Banks.BanksTitleDetails;
import com.lostingoa.lostingoa.Bike.BikeTitleDetails;
import com.lostingoa.lostingoa.Database.DatabaseInstance;
import com.lostingoa.lostingoa.Database.DatabaseUtils;
import com.lostingoa.lostingoa.Entertainment.EntertainmentTitleDetails;
import com.lostingoa.lostingoa.PostOffice.PostOfficeTitleDetails;
import com.lostingoa.lostingoa.R;
import com.lostingoa.lostingoa.Repair.RepairTitleDetails;
import com.lostingoa.lostingoa.Taxi.TaxiTitleDetails;
import com.lostingoa.lostingoa.Transport.TransportTitleDetails;
import com.lostingoa.lostingoa.Utils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ramakant on 30/6/16.
 */
public class EmergencyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    View mView;
    Context mContext;
    String moduleKey;
    public EmergencyViewHolder(View itemView) {
        super(itemView);
        mView = itemView;
        mContext = itemView.getContext();
        itemView.setOnClickListener(this);
    }

    public void bindEmergency(EmergencyModel emergencyModel){
        TextView tvName = (TextView) mView.findViewById(R.id.em_title);

        tvName.setText(emergencyModel.getTitle());
    }

    public void bindEmergency(EmergencyModel emergencyModel, String moduleKey){
        this.moduleKey = moduleKey;
        TextView tvName = (TextView) mView.findViewById(R.id.em_title);
        tvName.setText(emergencyModel.getTitle());
    }
    @Override
    public void onClick(final View view) {
        final ArrayList<String> list = new ArrayList<>();
        Intent intent = null;/* = new Intent(mContext, EmergencyDetails.class);*/
        DatabaseReference ref = null;/*= DatabaseInstance.getFirebaseInstance().getReference(DatabaseUtils.EMERGENCY_TABLE_NAME);*/
        if(moduleKey.equals(Utils.ADD_EMERGENCY_TITLE)){
            ref = DatabaseInstance.getFirebaseInstance().getReference(DatabaseUtils.EMERGENCY_TABLE_NAME);
            intent = new Intent(mContext, EmergencyDetails.class);

        }else if(moduleKey.equals(Utils.ADD_TRANSPORT_TITLE)){
            ref = DatabaseInstance.getFirebaseInstance().getReference(DatabaseUtils.TRANSPORT_TABLE_NAME);
            intent = new Intent(mContext, TransportTitleDetails.class);

        }else if(moduleKey.equals(Utils.ADD_BANKS_TITLE)){
            ref = DatabaseInstance.getFirebaseInstance().getReference(DatabaseUtils.BANKS_TABLE_NAME);
            intent = new Intent(mContext, BanksTitleDetails.class);

        }/*else if(moduleKey.equals(Utils.ADD_POST_OFFICE_TITLE)){
            ref = DatabaseInstance.getFirebaseInstance().getReference(DatabaseUtils.POST_OFFICE_TABLE_NAME);
            intent = new Intent(mContext, PostOfficeTitleDetails.class);

        }*/else if(moduleKey.equals(Utils.ADD_TAXI_TITLE)){
            ref = DatabaseInstance.getFirebaseInstance().getReference(DatabaseUtils.TAXI_TABLE_NAME);
            intent = new Intent(mContext, TaxiTitleDetails.class);

        }else if(moduleKey.equals(Utils.ADD_BIKE_TITLE)){
            ref = DatabaseInstance.getFirebaseInstance().getReference(DatabaseUtils.BIKE_PILOT_TABLE_NAME);
            intent = new Intent(mContext, BikeTitleDetails.class);

        }else if(moduleKey.equals(Utils.ADD_ENTERTAINMENT_TITLE)){
            ref = DatabaseInstance.getFirebaseInstance().getReference(DatabaseUtils.ENTERTAINMENT_TABLE_NAME);
            intent = new Intent(mContext, EntertainmentTitleDetails.class);

        }else if(moduleKey.equals(Utils.ADD_REPAIR_TITLE)){
            ref = DatabaseInstance.getFirebaseInstance().getReference(DatabaseUtils.REPAIR_TABLE_NAME);
            intent = new Intent(mContext, RepairTitleDetails.class);

        }else if(moduleKey.equals(Utils.ADD_CLASSIFIEDS_TITLE)){
            ref = DatabaseInstance.getFirebaseInstance().getReference(DatabaseUtils.CLASSIFIEDS_TABLE_NAME);

        }


        if(ref != null){
            final Intent finalIntent = intent;
            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        //map = new HashMap<>();
                        //map.put(snapshot.getKey(),snapshot.getValue(EmergencyModel.class));
                        list.add((snapshot.getValue(EmergencyModel.class).getTitle()));
                        //map = null;
                    }
                    int itemPosition = getLayoutPosition();
                    //Intent intent = new Intent(mContext, EmergencyDetails.class);
                    finalIntent.putExtra("position", itemPosition+"");
                    finalIntent.putExtra("list", list);
                    mContext.startActivity(finalIntent);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Toast.makeText(mContext,"error",Toast.LENGTH_LONG).show();
                }
            });
        }else {
            //Toast.makeText(mContext,"Null Ref",Toast.LENGTH_LONG).show();
        }

    }
}
