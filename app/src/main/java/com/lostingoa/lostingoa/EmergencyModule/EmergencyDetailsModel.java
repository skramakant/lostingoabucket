package com.lostingoa.lostingoa.EmergencyModule;

import android.widget.ImageView;

/**
 * Created by ramakant on 30/6/16.
 */
public class EmergencyDetailsModel {

    public String getEmName() {
        return emName;
    }

    public void setEmName(String emName) {
        this.emName = emName;
    }

    public String getEmNumber() {
        return emNumber;
    }

    public void setEmNumber(String emNumber) {
        this.emNumber = emNumber;
    }

    public String getEmDetails() {
        return emDetails;
    }

    public void setEmDetails(String emDetails) {
        this.emDetails = emDetails;
    }

    private String emName;
    private String emNumber;
    private String emDetails;

    public EmergencyDetailsModel(){}

    public EmergencyDetailsModel(String emName , String emNumber, String emDetails){
        this.emName = emName;
        this.emNumber = emNumber;
        this.emDetails = emDetails;
    }
}
