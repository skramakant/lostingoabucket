package com.lostingoa.lostingoa.EmergencyModule;

/**
 * Created by ramakant on 1/7/16.
 */
public class EmergencyModel {

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    private String Title;

    public EmergencyModel(){}

    public EmergencyModel(String title){
        this.Title = title;
    }
}
