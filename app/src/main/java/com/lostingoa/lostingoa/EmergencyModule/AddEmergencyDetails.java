package com.lostingoa.lostingoa.EmergencyModule;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.lostingoa.lostingoa.Database.DatabaseInstance;
import com.lostingoa.lostingoa.Database.DatabaseUtils;
import com.lostingoa.lostingoa.R;
import com.lostingoa.lostingoa.Utils;

public class AddEmergencyDetails extends AppCompatActivity implements View.OnClickListener {
    public static String TAG = AddEmergencyDetails.class.getSimpleName();
    private EditText etTitle;
    private EditText etNumber;
    private EditText etDetails;
    private Button btAdd;


    private DatabaseReference mDatabase;
    private String detailsKey;
    private String moduleKey;
    private String tableRoot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_emergency_details);
        initializeViews();
        detailsKey = getIntent().getStringExtra("key");
        moduleKey = getIntent().getStringExtra(Utils.KEY_ADD_DETAILS);
        mDatabase = DatabaseInstance.getFirebaseInstance().getReference();
    }

    private void initializeViews() {
        etTitle = (EditText) findViewById(R.id.emEtTitle);
        etNumber = (EditText) findViewById(R.id.emEtNumber);
        etDetails = (EditText) findViewById(R.id.emEtDetails);
        btAdd = (Button) findViewById(R.id.AddEmergency);
        btAdd.setOnClickListener(this);
    }


    private void addEmergency(EmergencyDetailsModel order) {
        // mDatabase.child("orders").child(userId).setValue(order);
        //Map<String, Object> postValues = order.toMap();

        String key = null;

        if(moduleKey.equals(Utils.ADD_EMERGENCY_DETAILS)){
            key = mDatabase.child(DatabaseUtils.EMERGENCY_TABLE_DETAILS_NAME).child(detailsKey).push().getKey();
            tableRoot = DatabaseUtils.EMERGENCY_TABLE_DETAILS_NAME;

        }else if(moduleKey.equals(Utils.ADD_SNAKE_RESCUER_DETAILS)){
            key = mDatabase.child(DatabaseUtils.SNAKE_RESCUER_DETAILS_TABLE_NAME).child(detailsKey).push().getKey();
            tableRoot = DatabaseUtils.SNAKE_RESCUER_DETAILS_TABLE_NAME;

        }else if(moduleKey.equals(Utils.ADD_SAFETY_POLICE_DETAILS)){
            key = mDatabase.child(DatabaseUtils.SAFETY_POLICE_DETAILS_TABLE_NAME).push().getKey();
            tableRoot = DatabaseUtils.SAFETY_POLICE_DETAILS_TABLE_NAME;

        }else if(moduleKey.equals(Utils.ADD_TRANSPORT_DETAILS)){
            key = mDatabase.child(DatabaseUtils.TRANSPORT_DETAILS_TABLE_NAME).child(detailsKey).push().getKey();
            tableRoot = DatabaseUtils.TRANSPORT_DETAILS_TABLE_NAME;

        }else if(moduleKey.equals(Utils.ADD_BANKS_DETAILS)){
            key = mDatabase.child(DatabaseUtils.BANKS_DETAILS_TABLE_NAME).child(detailsKey).push().getKey();
            tableRoot = DatabaseUtils.BANKS_DETAILS_TABLE_NAME;

        }else if(moduleKey.equals(Utils.ADD_POST_OFFICE_DETAILS)){
            key = mDatabase.child(DatabaseUtils.POST_OFFICE_DETAILS_TABLE_NAME).child(detailsKey).push().getKey();
            tableRoot = DatabaseUtils.POST_OFFICE_DETAILS_TABLE_NAME;

        }else if(moduleKey.equals(Utils.ADD_TAXI_DETAILS)){
            key = mDatabase.child(DatabaseUtils.TAXI_DETAILLS_TABLE_NAME).child(detailsKey).push().getKey();
            tableRoot = DatabaseUtils.TAXI_DETAILLS_TABLE_NAME;

        }else if(moduleKey.equals(Utils.ADD_BIKE_DETAILS)){
            key = mDatabase.child(DatabaseUtils.BIKE_PILOT_DETAILS_TABLE_NAME).child(detailsKey).push().getKey();
            tableRoot = DatabaseUtils.BIKE_PILOT_DETAILS_TABLE_NAME;

        }else if(moduleKey.equals(Utils.ADD_ENTERTAINMENT_DETAILS)){
            key = mDatabase.child(DatabaseUtils.ENTERTAINMENT_DETAILS_TABLE_NAME).child(detailsKey).push().getKey();
            tableRoot = DatabaseUtils.ENTERTAINMENT_DETAILS_TABLE_NAME;

        }else if(moduleKey.equals(Utils.ADD_REPAIR_DETAILS)){
            key = mDatabase.child(DatabaseUtils.REPAIR_DETAILS_TABLE_NAME).child(detailsKey).push().getKey();
            tableRoot = DatabaseUtils.REPAIR_DETAILS_TABLE_NAME;

        }else if(moduleKey.equals(Utils.ADD_CLASSIFIEDS_DETAILS)){
            key = mDatabase.child(DatabaseUtils.CLASSIFIEDS_DETAILS_TABLE_NAME).child(detailsKey).push().getKey();
            tableRoot = DatabaseUtils.CLASSIFIEDS_DETAILS_TABLE_NAME;
        }


        //Map<String, Object> childUpdates = new HashMap<>();

        //childUpdates.put("/orders/" + key, postValues);
        //childUpdates.put("/user-orders/" + userId + "/" + key, postValues);

        //mDatabase.updateChildren(childUpdates);

        if(moduleKey.equals(Utils.ADD_SAFETY_POLICE_DETAILS)){
            mDatabase.child(tableRoot).child(key).setValue(order, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if(databaseError != null){
                        Toast.makeText(getApplication(),databaseError.toString(),Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(getApplication(),"order placed successfully",Toast.LENGTH_LONG).show();
                        finish();
                    }
                }
            });
        }else {
            mDatabase.child(tableRoot).child(detailsKey).child(key).setValue(order, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if(databaseError != null){
                        Toast.makeText(getApplication(),databaseError.toString(),Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(getApplication(),"order placed successfully",Toast.LENGTH_LONG).show();
                        finish();
                    }
                }
            });
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.AddEmergency:
                EmergencyDetailsModel emergency = new EmergencyDetailsModel();
                emergency.setEmName(etTitle.getText().toString());
                emergency.setEmNumber(etNumber.getText().toString());
                emergency.setEmDetails(etDetails.getText().toString());
                addEmergency(emergency);

        }
    }
}
