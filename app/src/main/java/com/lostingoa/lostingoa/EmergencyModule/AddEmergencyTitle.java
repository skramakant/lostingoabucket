package com.lostingoa.lostingoa.EmergencyModule;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.lostingoa.lostingoa.Database.DatabaseInstance;
import com.lostingoa.lostingoa.Database.DatabaseUtils;
import com.lostingoa.lostingoa.R;
import com.lostingoa.lostingoa.Utils;

public class AddEmergencyTitle extends AppCompatActivity implements View.OnClickListener{

    private TextView tvTitle;
    private Button btSend;
    private EditText etTitle;
    private String moduleKey;
    private String tableRoot;

    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_emergency_title);

        initializeView();
        moduleKey = getIntent().getStringExtra(Utils.KEY_ADD_TITLE);
        mDatabase = DatabaseInstance.getFirebaseInstance().getReference();
    }

    private void initializeView() {
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        etTitle = (EditText) findViewById(R.id.etTitle);
        btSend = (Button) findViewById(R.id.btSend);
        btSend.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btSend:

                if(etTitle.getText().toString().equals("")){
                    Toast.makeText(this,"Enter Title",Toast.LENGTH_LONG).show();
                }else {
                    EmergencyModel emergencyTitle = new EmergencyModel();
                    emergencyTitle.setTitle(etTitle.getText().toString().toUpperCase());
                    uploadToFirebase(emergencyTitle);
                }
                break;
        }
    }

    private void uploadToFirebase(EmergencyModel title) {

        // mDatabase.child("orders").child(userId).setValue(order);
        //Map<String, Object> postValues = order.toMap();
        String key = null;
        if(moduleKey.equals(Utils.ADD_EMERGENCY_TITLE)){
            key = mDatabase.child(DatabaseUtils.EMERGENCY_TABLE_NAME).push().getKey();
            tableRoot = DatabaseUtils.EMERGENCY_TABLE_NAME;
        }else if(moduleKey.equals(Utils.ADD_SNAKE_RESCUER_TITLE)){
            key = mDatabase.child(DatabaseUtils.SNAKE_RESCUER_TABLE_NAME).push().getKey();
            tableRoot = DatabaseUtils.SNAKE_RESCUER_TABLE_NAME;
        }else if(moduleKey.equals(Utils.ADD_TRANSPORT_TITLE)){
            key = mDatabase.child(DatabaseUtils.TRANSPORT_TABLE_NAME).push().getKey();
            tableRoot = DatabaseUtils.TRANSPORT_TABLE_NAME;
        }else if(moduleKey.equals(Utils.ADD_BANKS_TITLE)){
            key = mDatabase.child(DatabaseUtils.BANKS_TABLE_NAME).push().getKey();
            tableRoot = DatabaseUtils.BANKS_TABLE_NAME;
        }else if(moduleKey.equals(Utils.ADD_POST_OFFICE_TITLE)){
            key = mDatabase.child(DatabaseUtils.POST_OFFICE_TABLE_NAME).push().getKey();
            tableRoot = DatabaseUtils.POST_OFFICE_TABLE_NAME;
        }else if(moduleKey.equals(Utils.ADD_TAXI_TITLE)){
            key = mDatabase.child(DatabaseUtils.TAXI_TABLE_NAME).push().getKey();
            tableRoot = DatabaseUtils.TAXI_TABLE_NAME;
        }else if(moduleKey.equals(Utils.ADD_BIKE_TITLE)){
            key = mDatabase.child(DatabaseUtils.BIKE_PILOT_TABLE_NAME).push().getKey();
            tableRoot = DatabaseUtils.BIKE_PILOT_TABLE_NAME;
        }else if(moduleKey.equals(Utils.ADD_ENTERTAINMENT_TITLE)){
            key = mDatabase.child(DatabaseUtils.ENTERTAINMENT_TABLE_NAME).push().getKey();
            tableRoot = DatabaseUtils.ENTERTAINMENT_TABLE_NAME;
        }else if(moduleKey.equals(Utils.ADD_REPAIR_TITLE)){
            key = mDatabase.child(DatabaseUtils.REPAIR_TABLE_NAME).push().getKey();
            tableRoot = DatabaseUtils.REPAIR_TABLE_NAME;
        }else if(moduleKey.equals(Utils.ADD_CLASSIFIEDS_TITLE)){
            key = mDatabase.child(DatabaseUtils.CLASSIFIEDS_TABLE_NAME).push().getKey();
            tableRoot = DatabaseUtils.CLASSIFIEDS_TABLE_NAME;
        }

        //Map<String, Object> childUpdates = new HashMap<>();

        //childUpdates.put("/orders/" + key, postValues);
        //childUpdates.put("/user-orders/" + userId + "/" + key, postValues);

        //mDatabase.updateChildren(childUpdates);

        mDatabase.child(tableRoot).child(key).setValue(title, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError != null){
                    Toast.makeText(getApplication(),databaseError.toString(),Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(getApplication(),"item added successfully",Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        });
    }
}

