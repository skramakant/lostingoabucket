package com.lostingoa.lostingoa.EmergencyModule;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lostingoa.lostingoa.LostInGoaApplication;
import com.lostingoa.lostingoa.R;

/**
 * Created by ramakant on 2/7/16.
 */
public class EmergencyDetailsViewHolder extends RecyclerView.ViewHolder {
    View mView;
    Context mContext;
    private TextView tvName;
    private TextView tvNumber;
    private TextView tvDetails;
    private ImageView ivCall;
    public EmergencyDetailsViewHolder(View itemView) {
        super(itemView);
        mView = itemView;
        mContext = itemView.getContext();
        tvName = (TextView) mView.findViewById(R.id.em_name);
        tvNumber = (TextView) mView.findViewById(R.id.em_number);
        tvDetails = (TextView) mView.findViewById(R.id.em_details);
        ivCall = (ImageView) mView.findViewById(R.id.iv_call);
    }

    public void bindEmergency(EmergencyDetailsModel emergencyModel){
        tvName.setText(emergencyModel.getEmName());
        tvNumber.setText(emergencyModel.getEmNumber());
        tvDetails.setText(emergencyModel.getEmDetails());
        ivCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LostInGoaApplication.phoneCall(mContext,tvNumber.getText().toString().trim());
            }
        });
    }
}
