package com.lostingoa.lostingoa.FeedBack;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.lostingoa.lostingoa.Database.DatabaseInstance;
import com.lostingoa.lostingoa.Database.DatabaseUtils;
import com.lostingoa.lostingoa.Feed.UserHandleForFeed;
import com.lostingoa.lostingoa.R;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class FeedBack extends AppCompatActivity implements View.OnClickListener{

    private EditText etFeedBack;
    private Button btSubmitFeedBack;
    private DatabaseReference mDatabase;
    private DatabaseReference mGoaFeedReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);
        getSupportActionBar().setTitle("FeedBack");
        mDatabase = DatabaseInstance.getFirebaseInstance().getReference();
        mGoaFeedReference = DatabaseInstance.getFirebaseInstance().getReference().child(DatabaseUtils.GOA_FEED_TABLE_NAME);

        initializeViews();
    }

    private void initializeViews() {
        etFeedBack = (EditText) findViewById(R.id.etFeedBack);
        btSubmitFeedBack = (Button) findViewById(R.id.btSubmitFeedBack);
        btSubmitFeedBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btSubmitFeedBack:
                String feedBackText = etFeedBack.getText().toString();
                if(feedBackText.equals("")){
                    Toast.makeText(this,"Please write Something",Toast.LENGTH_LONG).show();
                }else {
                    uploadFeedBackToServer(feedBackText);
                }
                break;
        }
    }

    private void uploadFeedBackToServer(String feedBackText) {
        String key = mDatabase.child(DatabaseUtils.FEED_BACK_TABLE_NAME).push().getKey();
        mDatabase.child(DatabaseUtils.FEED_BACK_TABLE_NAME).child(key).setValue(feedBackText);
        Toast.makeText(getApplication(),"Thank You!",Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.user_feed_back_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.admin) {
            deleteOlderMessage();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * this message is only for ADMIN
     */
    private void deleteOlderMessage() {
        long cutoff = System.currentTimeMillis() - TimeUnit.MILLISECONDS.convert(12, TimeUnit.HOURS);
        Query oldItems = mGoaFeedReference.orderByChild("timeStamp").endAt(cutoff);
        oldItems.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot itemSnapshot: snapshot.getChildren()) {
                    itemSnapshot.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
}
