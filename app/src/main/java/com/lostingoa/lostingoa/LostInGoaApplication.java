package com.lostingoa.lostingoa;

import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.widget.ProgressBar;

import com.google.firebase.messaging.FirebaseMessaging;
import com.instamojo.android.Instamojo;
import com.lostingoa.lostingoa.Notification.NotificationModel;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by ramakant on 17/9/16.
 */
public class LostInGoaApplication extends Application {

    public static ProgressDialog progressDialog;
    @Override
    public void onCreate() {
        super.onCreate();
        Instamojo.initialize(this);
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(config);
        FirebaseMessaging.getInstance().subscribeToTopic("allDevicesv3.0");
        //FirebaseMessaging.getInstance().subscribeToTopic("allDevicesv2.2");
        FirebaseMessaging.getInstance().subscribeToTopic("allDevices");
    }

    public static void phoneCall(Context context, String number) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }else {
            context.startActivity(intent);
        }

    }

    public static boolean isInternetConnected(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void saveInSharedPreference(Context context,String key, String value){
        SharedPreferences sharedPref = context.getSharedPreferences(Utils.APP_SHARED_PREFERENCE_FILE,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getFromSharedPreference(Context context, String key){
        SharedPreferences sharedPref = context.getSharedPreferences(Utils.APP_SHARED_PREFERENCE_FILE,Context.MODE_PRIVATE);
        String result = sharedPref.getString(key,Utils.DEFAULT_HANDLE);
        return result;
    }

    public static void showProgressDialog(Context context){
        if(true){
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Loading...");
//            progressDialog.setMax(100);
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//            progressDialog.setIndeterminate(true);
//            progressDialog.setProgressNumberFormat(null);
//            progressDialog.setProgressPercentFormat(null);
        }
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }else {
            progressDialog.show();
        }
    }

    public static void hideProgressDialog(){
        if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }

}
