package com.lostingoa.lostingoa.Notification;

import io.realm.RealmObject;

/**
 * Created by ramakant on 30/1/17.
 */
public class NotificationModel extends RealmObject{
    private String title;

    private String content;

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    private long timeStamp;

    public NotificationModel(){};
    public NotificationModel(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
