package com.lostingoa.lostingoa.Notification;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lostingoa.lostingoa.R;

import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by ramakant on 30/1/17.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {

    private List<NotificationModel> notifications;

    public NotificationAdapter(List<NotificationModel> notifications) {
        this.notifications = notifications;
    }

    @Override
    public NotificationAdapter.NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_notification_item, parent, false);

        return new NotificationViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.NotificationViewHolder holder, int position) {
        NotificationModel notification = notifications.get(position);
        TextView title = holder.titleRef.get();
        TextView content = holder.contentRef.get();
        TextView date = holder.dateRef.get();
        if (notification != null) {
            if (title != null) {
                title.setText(notification.getTitle());
            }
            if (content != null) {
                content.setText(notification.getContent());
            }
            if (date != null) {
                date.setText(formatDate(notification.getTimeStamp()));
            }
        }
    }

    @Override
    public int getItemCount() {
        if (notifications.size() == 0) {
            return 0;
        }
        return notifications.size();
    }

    public static class NotificationViewHolder extends RecyclerView.ViewHolder{

        private WeakReference<TextView> titleRef, contentRef,dateRef;

        public NotificationViewHolder(View itemView) {
            super(itemView);
            TextView title, content,dateTime;
            title = (TextView) itemView.findViewById(R.id.tvTitle);
            content = (TextView) itemView.findViewById(R.id.tvContent);
            dateTime = (TextView) itemView.findViewById(R.id.dateTime);
            titleRef = new WeakReference<TextView>(title);
            contentRef = new WeakReference<TextView>(content);
            dateRef = new WeakReference<TextView>(dateTime);
        }
    }

    public String formatDate(long dateInMillis) {
        Date date = new Date(dateInMillis);
        return DateFormat.getDateInstance().format(date);
    }
}