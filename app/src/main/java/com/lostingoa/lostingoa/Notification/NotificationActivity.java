package com.lostingoa.lostingoa.Notification;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.lostingoa.lostingoa.Dashboard;
import com.lostingoa.lostingoa.R;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by ramakant on 26/1/17.
 */
public class NotificationActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private NotificationAdapter mAdapter;
    private List<NotificationModel> mNotifications;
    private final int NOTIFICATIONS_LIMIT = 20;
    public Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        getSupportActionBar().setTitle("Notification");
        realm = Realm.getDefaultInstance();
        deleteOldNotificationFromDatabase();
        mNotifications = new ArrayList<>();
        if (getIntent().getExtras() != null) {
            Bundle msg = getIntent().getExtras().getBundle("NOTIFY");
            if(msg != null && msg.get("msgTitle")!=null && msg.get("msgBody")!=null){
                realm.beginTransaction();
                NotificationModel notification = realm.createObject(NotificationModel.class);
                notification.setTitle(String.valueOf(msg.get("msgTitle")));
                notification.setContent(String.valueOf(msg.get("msgBody")));
                notification.setTimeStamp(System.currentTimeMillis());
                //mNotifications.add(notification);
                realm.commitTransaction();
            }
        }
/*        for(int i=0;i<20;i++){
            realm.beginTransaction();
            NotificationModel notification = realm.createObject(NotificationModel.class);
            notification.setTitle("LostInGoa");
            notification.setContent("Hello Goa");
            realm.commitTransaction();
        }*/

        RealmResults<NotificationModel> notification = realm.where(NotificationModel.class).findAll();
        notification=notification.sort("timeStamp",Sort.DESCENDING);
        if(notification != null){
            mNotifications = notification;
        }
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new NotificationAdapter(mNotifications);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        //realm.close();
    }

    private void deleteOldNotificationFromDatabase() {
        final RealmResults<NotificationModel> deleteNotification = realm.where(NotificationModel.class).findAll();
        long cutoff = System.currentTimeMillis() - TimeUnit.MILLISECONDS.convert(30, TimeUnit.DAYS);
        for(int i=0;i< deleteNotification.size();i++ ){
            NotificationModel notify = deleteNotification.get(i);
            if(notify.getTimeStamp() <= cutoff){
                final int finalI = i;
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        //notify.deleteFromRealm();
                        deleteNotification.deleteFromRealm(finalI);
                    }
                });
            }
        }
    }

/*    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, Dashboard.class);
        startActivity(intent);
    }*/
}
