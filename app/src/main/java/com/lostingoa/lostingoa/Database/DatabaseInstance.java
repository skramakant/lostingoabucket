package com.lostingoa.lostingoa.Database;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by ramakant on 2/7/16.
 */
public class DatabaseInstance {

    private static FirebaseDatabase firebaseInstance;

    private DatabaseInstance(){}

    private static FirebaseDatabase createInstance(){
        if(firebaseInstance == null){
            firebaseInstance = FirebaseDatabase.getInstance();
            firebaseInstance.setPersistenceEnabled(true);
        }
        return firebaseInstance;
    }
    public static FirebaseDatabase getFirebaseInstance(){
        return createInstance();
    }
}
