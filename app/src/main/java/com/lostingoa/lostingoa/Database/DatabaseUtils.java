package com.lostingoa.lostingoa.Database;

/**
 * Created by ramakant on 2/7/16.
 */
public class DatabaseUtils {

    public static String EMERGENCY_TABLE_NAME = "emergency";
    public static String EMERGENCY_TABLE_DETAILS_NAME = "emergency_details";

    public static String SNAKE_RESCUER_TABLE_NAME = "snake_rescuer";
    public static String SNAKE_RESCUER_DETAILS_TABLE_NAME = "snake_rescuer_details";

    public static String SAFETY_POLICE_TABLE_NAME = "safety_police";
    public static String SAFETY_POLICE_DETAILS_TABLE_NAME = "safety_police_details";

    public static String TRANSPORT_TABLE_NAME = "transport";
    public static String TRANSPORT_DETAILS_TABLE_NAME = "transport_details";

    public static String BANKS_TABLE_NAME = "banks";
    public static String BANKS_DETAILS_TABLE_NAME = "banks_details";

    public static String POST_OFFICE_TABLE_NAME = "post_office";
    public static String POST_OFFICE_DETAILS_TABLE_NAME = "post_office_details";

    public static String TAXI_TABLE_NAME = "taxi";
    public static String TAXI_DETAILLS_TABLE_NAME = "taxi_details";

    public static String BIKE_PILOT_TABLE_NAME = "bike";
    public static String BIKE_PILOT_DETAILS_TABLE_NAME = "bike_details";

    public static String ENTERTAINMENT_TABLE_NAME = "entertainment";
    public static String ENTERTAINMENT_DETAILS_TABLE_NAME = "entertainment_details";

    public static String REPAIR_TABLE_NAME = "repair";
    public static String REPAIR_DETAILS_TABLE_NAME = "repair_details";

    public static String CLASSIFIEDS_TABLE_NAME = "classifieds";
    public static String CLASSIFIEDS_DETAILS_TABLE_NAME = "classifieds_details";

    public static String FEED_BACK_TABLE_NAME = "feedback";
    public static String GOA_FEED_TABLE_NAME = "goafeed";

    public static String GOA_CLASSIFIEDS_TABLE_NAME = "classifieds";



    public DatabaseUtils(){}

}
