package com.lostingoa.lostingoa.Transport;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.lostingoa.lostingoa.Database.DatabaseInstance;
import com.lostingoa.lostingoa.Database.DatabaseUtils;
import com.lostingoa.lostingoa.EmergencyModule.AddEmergencyDetails;
import com.lostingoa.lostingoa.EmergencyModule.EmergencyDetailsModel;
import com.lostingoa.lostingoa.EmergencyModule.EmergencyDetailsViewHolder;
import com.lostingoa.lostingoa.LostInGoaApplication;
import com.lostingoa.lostingoa.MapView;
import com.lostingoa.lostingoa.R;
import com.lostingoa.lostingoa.Utils;

import java.util.ArrayList;

public class TransportTitleDetails extends AppCompatActivity {
    private RecyclerView emList;
    private DatabaseReference mEmergencyReference;
    private FirebaseRecyclerAdapter mFirebaseAdapter;
    String detailsKey;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transport_title_details);
        initializeViews();
        Intent current = getIntent();
        int position = Integer.parseInt(current.getStringExtra("position"));
        ArrayList<String> list = (ArrayList<String>) current.getSerializableExtra("list");
        detailsKey = list.get(position);
        getSupportActionBar().setTitle(detailsKey);
        detailsKey = detailsKey.toUpperCase();
        mEmergencyReference = DatabaseInstance.getFirebaseInstance().getReference().child(DatabaseUtils.TRANSPORT_DETAILS_TABLE_NAME).child(detailsKey);
        mEmergencyReference.keepSynced(true);
        setUpFirebaseAdapter();
    }

    private void setUpFirebaseAdapter() {
        LostInGoaApplication.showProgressDialog(this);
        mFirebaseAdapter = new FirebaseRecyclerAdapter<EmergencyDetailsModel,EmergencyDetailsViewHolder>(EmergencyDetailsModel.class, R.layout.emergency_list_item, EmergencyDetailsViewHolder.class,
                mEmergencyReference) {

            @Override
            protected void populateViewHolder(EmergencyDetailsViewHolder viewHolder, EmergencyDetailsModel model, int position) {
                viewHolder.bindEmergency(model);
            }
        };
        emList.setHasFixedSize(true);
        emList.setLayoutManager(new LinearLayoutManager(this));
        emList.setAdapter(mFirebaseAdapter);
        mEmergencyReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                LostInGoaApplication.hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void initializeViews() {
        emList = (RecyclerView) findViewById(R.id.tvTransportDetailsList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.entertainment_map_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.map) {
            //return true;
/*            Intent intent = new Intent(this,AddEmergencyDetails.class);
            intent.putExtra("key",detailsKey);
            intent.putExtra(Utils.KEY_ADD_DETAILS,Utils.ADD_TRANSPORT_DETAILS);
            startActivity(intent);*/

            Intent intent = new Intent(this, MapView.class);
            intent.putExtra("map_key",detailsKey);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
