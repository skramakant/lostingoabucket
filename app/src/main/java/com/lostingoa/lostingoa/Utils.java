package com.lostingoa.lostingoa;

/**
 * Created by ramakant on 2/7/16.
 */
public class Utils {

    public static String KEY_ADD_TITLE = "add_title";
    public static String KEY_ADD_DETAILS = "add_details";

    public static String ADD_EMERGENCY_TITLE = "add_emergency_title";
    public static String ADD_EMERGENCY_DETAILS = "add_emergency_details";

    public static String ADD_SNAKE_RESCUER_TITLE = "add_snake_rescuer_title";
    public static String ADD_SNAKE_RESCUER_DETAILS = "add_snake_rescuer_details";

    public static String ADD_SAFETY_POLICE_DETAILS = "add_safety_police_details";

    public static String ADD_TRANSPORT_TITLE = "add_transport_title";
    public static String ADD_TRANSPORT_DETAILS = "add_transport_details";

    public static String ADD_BANKS_TITLE = "add_banks_title";
    public static String ADD_BANKS_DETAILS = "add_banks_details";

    public static String ADD_POST_OFFICE_TITLE = "add_post_office_title";
    public static String ADD_POST_OFFICE_DETAILS = "add_post_office_details";

    public static String ADD_TAXI_TITLE = "add_taxi_title";
    public static String ADD_TAXI_DETAILS = "add_taxi_details";

    public static String ADD_BIKE_TITLE = "add_bike_title";
    public static String ADD_BIKE_DETAILS = "add_bike_details";

    public static String ADD_ENTERTAINMENT_TITLE = "add_entertainment_title";
    public static String ADD_ENTERTAINMENT_DETAILS = "add_entertainment_details";

    public static String ADD_REPAIR_TITLE = "add_repair_title";
    public static String ADD_REPAIR_DETAILS = "add_repair_details";

    public static String ADD_CLASSIFIEDS_TITLE = "add_classifieds_title";
    public static String ADD_CLASSIFIEDS_DETAILS = "add_classifieds_details";

    public static String APP_SHARED_PREFERENCE_FILE = "App_Shared_Preference_file";
    public static String USER_HANDLE_KEY = "USER_HANDLE";
    public static String DEFAULT_HANDLE = "UNKNOWN";

}
