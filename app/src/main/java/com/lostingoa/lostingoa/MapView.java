package com.lostingoa.lostingoa;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.CellInfo;
import android.telephony.TelephonyManager;

import java.util.List;

public class MapView extends AppCompatActivity {

    private TouchImageView touchImageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_view);
        touchImageView = (TouchImageView) findViewById(R.id.map_view);
        String mapKey = getIntent().getStringExtra("map_key").trim();

        if(mapKey != null && mapKey.equals("BEACHES")){
            touchImageView.setImageDrawable(ActivityCompat.getDrawable(this,R.drawable.goa_beaches));
        }else if(mapKey != null && mapKey.equals("WATERFALLS")){
            touchImageView.setImageDrawable(ActivityCompat.getDrawable(this,R.drawable.waterfalls));
        }else if(mapKey != null && mapKey.equals("FORTS")){
            touchImageView.setImageDrawable(ActivityCompat.getDrawable(this,R.drawable.forts_goa));
        }else if(mapKey != null && mapKey.equals("WILDLIFE SANCTUARIES")){
            touchImageView.setImageDrawable(ActivityCompat.getDrawable(this,R.drawable.wildlife_sanctuary_goa));
        }else if(mapKey != null && mapKey.equals("CHURCHES")){
            touchImageView.setImageDrawable(ActivityCompat.getDrawable(this,R.drawable.churches));
        }else if(mapKey != null && mapKey.equals("TEMPLES")){
            touchImageView.setImageDrawable(ActivityCompat.getDrawable(this,R.drawable.temples_goa));
        }else if(mapKey != null && mapKey.equals("ROAD")){
            touchImageView.setImageDrawable(ActivityCompat.getDrawable(this,R.drawable.goa_road_map));
        }else if(mapKey != null && mapKey.equals("RAIL")){
            touchImageView.setImageDrawable(ActivityCompat.getDrawable(this,R.drawable.goa_railway_map));
        }else {
            touchImageView.setImageDrawable(ActivityCompat.getDrawable(this,R.drawable.goa_tourist));
        }
    }
}
